package com.example.cocktailapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.example.cocktailapp.adapter.DrinkByCategoryAdapter
import com.example.cocktailapp.databinding.FragmentDrinkByCategoryBinding
import com.example.cocktailapp.viewModel.DrinkByCategoryViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class DrinkByCategoryFragment : Fragment() {

    private var _binding: FragmentDrinkByCategoryBinding? = null
    private val binding get() = _binding!!
    private val drinkByCategoryAdapter by lazy { DrinkByCategoryAdapter() }
    private val drinkByCategoryViewModel: DrinkByCategoryViewModel by viewModel()
    private val args by navArgs<DrinkByCategoryFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDrinkByCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initViews() = with(binding) {
        val categoryDrink = args.category
        rvDrinks.adapter = drinkByCategoryAdapter
        drinkByCategoryViewModel.getDrinkByCategory(categoryDrink)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            drinkByCategoryViewModel.categoryDrink.collectLatest { state ->
                drinkByCategoryAdapter.updateDrink(state.drinkList)

            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}