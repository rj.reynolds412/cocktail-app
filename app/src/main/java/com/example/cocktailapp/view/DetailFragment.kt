package com.example.cocktailapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.cocktailapp.databinding.FragmentDetailBinding
import com.example.cocktailapp.model.response.DetailByIdResponse.Details
import com.example.cocktailapp.viewModel.DetailsByIdViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val detailsByIdViewModel : DetailsByIdViewModel by viewModel()
    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
    }

    private fun initViews() = with(binding) {
        val detailArgs = args.id
        detailsByIdViewModel.getDetailsById(detailArgs)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            detailsByIdViewModel.details.collectLatest { state ->
                state.detailsById
                val details : Details = state.detailsById
            displayDetails(details)
            }
        }
    }

    private fun displayDetails(detail: Details) = with(binding) {
        detail.run {
            Glide.with(root.context)
                .load(detail.strDrinkThumb)
                .into(ivDetail)
            tvDetailTitle.text = detail.strDrink

            val detail = listOfNotNull(
                strIBA,
                strIngredient1,
                strIngredient2,
                strIngredient3,
                strIngredient4,
                strIngredient5,
                strIngredient6,
                strIngredient7,
                strIngredient8,
                strIngredient9,
                strIngredient10,
                strIngredient11,
                strIngredient12,
                strIngredient13,
                strIngredient14,
                strIngredient15,
                strMeasure1,
                strMeasure2,
                strMeasure3,
                strMeasure4,
                strMeasure5,
                strMeasure6,
                strMeasure7,
                strMeasure8,
                strMeasure9,
                strMeasure10,
                strMeasure11,
                strMeasure12,
                strMeasure13,
                strMeasure14,
                strMeasure15,
                strInstructions
            )
            tvDetail.text = detail.filter { it.isNotBlank() }.joinToString("\n")
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}