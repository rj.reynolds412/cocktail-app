package com.example.cocktailapp.koin

import com.example.cocktailapp.domain.GetCategoryUseCase
import com.example.cocktailapp.domain.GetDetailByIdUseCase
import com.example.cocktailapp.domain.GetDrinkByCategoryUseCase
import com.example.cocktailapp.model.CocktailRepo
import com.example.cocktailapp.model.CocktailService
import com.example.cocktailapp.model.LoggingInterceptor
import com.example.cocktailapp.utils.BASE_URL
import com.example.cocktailapp.viewModel.CategoryViewModel
import com.example.cocktailapp.viewModel.DetailsByIdViewModel
import com.example.cocktailapp.viewModel.DrinkByCategoryViewModel
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create


object CocktailModule {

    val appModule = module {

        single<CocktailService> {
            val okHttpClient = OkHttpClient.Builder().addInterceptor(LoggingInterceptor()).build()
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
            retrofit.create()
        }

        single { CocktailRepo(get()) }

        single { GetDetailByIdUseCase(get()) }

        single { GetCategoryUseCase(get()) }

        single { GetDrinkByCategoryUseCase(get()) }

        viewModel { CategoryViewModel(get()) }

        viewModel { DrinkByCategoryViewModel(get()) }

        viewModel { DetailsByIdViewModel(get()) }

    }
}