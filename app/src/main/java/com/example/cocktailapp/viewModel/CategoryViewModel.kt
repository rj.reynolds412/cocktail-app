package com.example.cocktailapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailapp.domain.GetCategoryUseCase
import com.example.cocktailapp.state.CategoryState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CategoryViewModel(private val getCategoryUseCase: GetCategoryUseCase)
    : ViewModel() {

    private val _category = MutableStateFlow(CategoryState(isLoading = true))
    val category = _category.asStateFlow()

    fun getCategory(category: String) {
        viewModelScope.launch {
            val result = getCategoryUseCase(category)
            if (result.isSuccess) {
                val categories = result.getOrDefault(listOf())
                _category.value = CategoryState(
                    isLoading = false,
                    categoryList = categories,
                )
            } else {
                _category.value = CategoryState(
                    isLoading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }

}