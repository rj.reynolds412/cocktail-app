package com.example.cocktailapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailapp.domain.GetDrinkByCategoryUseCase
import com.example.cocktailapp.state.DrinkByCategoryState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DrinkByCategoryViewModel(private val getDrinkByCategoryUseCase: GetDrinkByCategoryUseCase) :
    ViewModel() {

    private val _categoryDrink = MutableStateFlow(DrinkByCategoryState(isLoading = true))
    val categoryDrink = _categoryDrink.asStateFlow()

    fun getDrinkByCategory(categoryDrink: String) {
        viewModelScope.launch {
            val result = getDrinkByCategoryUseCase(categoryDrink)
            if (result.isSuccess) {
                val drinkByCategory = result.getOrDefault(listOf())
                _categoryDrink.value = DrinkByCategoryState(
                    isLoading = false,
                    drinkList = drinkByCategory,
                )
            } else {
                _categoryDrink.value = DrinkByCategoryState(
                    isLoading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}