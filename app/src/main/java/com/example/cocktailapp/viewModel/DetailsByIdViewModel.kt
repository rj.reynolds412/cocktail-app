package com.example.cocktailapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailapp.domain.GetDetailByIdUseCase
import com.example.cocktailapp.model.response.DetailByIdResponse
import com.example.cocktailapp.state.DetailsByIdState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.getScopeId
import org.koin.core.component.getScopeName

class DetailsByIdViewModel(private val getDetailByIdUseCase: GetDetailByIdUseCase) : ViewModel() {

    private val _details = MutableStateFlow(DetailsByIdState(isLoading = true))
    val details = _details.asStateFlow()


    fun getDetailsById(detailsById: Int) {
        viewModelScope.launch {
            val result: Result<DetailByIdResponse.Details> = getDetailByIdUseCase(detailsById)
            if (result.isSuccess) {
                val details = result.getOrNull()
                if (details != null) {
                    _details.value = DetailsByIdState(
                        isLoading = false,
                        detailsById = details
                    )
                }
            } else {
                _details.value = DetailsByIdState(
                    isLoading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}