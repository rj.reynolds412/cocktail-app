package com.example.cocktailapp.domain

import com.example.cocktailapp.model.CocktailRepo
import com.example.cocktailapp.model.response.DetailByIdResponse
import com.example.cocktailapp.model.response.DetailByIdResponse.Details
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDetailByIdUseCase(private val cocktailRepo: CocktailRepo) {

    suspend operator fun invoke(detailsById: Int): Result<Details> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val detailsBy: Details =
                    cocktailRepo.getDetailsById(detailsById).details.single()
                Result.success(detailsBy)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}