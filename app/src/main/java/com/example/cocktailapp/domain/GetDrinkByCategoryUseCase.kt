package com.example.cocktailapp.domain

import com.example.cocktailapp.model.CocktailRepo
import com.example.cocktailapp.model.response.DrinksByCategoryResponse.Drink
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkByCategoryUseCase(private val cocktailRepo: CocktailRepo) {

    suspend operator fun invoke(categoryDrink: String): Result<List<Drink>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val drinkByCategory: List<Drink> =
                    cocktailRepo.getDrinkByCategory(categoryDrink).drinks
                Result.success(drinkByCategory)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}