package com.example.cocktailapp.domain

import com.example.cocktailapp.model.CocktailRepo
import com.example.cocktailapp.model.response.CategoryResponse.Category
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetCategoryUseCase(private val cocktailRepo: CocktailRepo) {

    suspend operator fun invoke(category: String): Result<List<Category>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val response = cocktailRepo.getCategories(category)
                val drinkCategory: List<Category> = response.categories
                Result.success(drinkCategory)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}