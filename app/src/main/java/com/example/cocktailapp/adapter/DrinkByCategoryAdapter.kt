package com.example.cocktailapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cocktailapp.databinding.ItemDrinkBinding
import com.example.cocktailapp.model.response.DrinksByCategoryResponse.Drink
import com.example.cocktailapp.view.DrinkByCategoryFragmentDirections

class DrinkByCategoryAdapter :
    RecyclerView.Adapter<DrinkByCategoryAdapter.DrinkByCategoryViewHolder>() {
    private val drink = mutableListOf<Drink>()

    class DrinkByCategoryViewHolder(
         val binding: ItemDrinkBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun displayDrinks(categoryDrink: Drink) = with(binding) {
            Glide.with(root.context)
                .load(categoryDrink.strDrinkThumb)
                .into(ivDrink)
            tvDrink.text = categoryDrink.strDrink
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinkBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinkByCategoryViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkByCategoryViewHolder =
        DrinkByCategoryViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: DrinkByCategoryViewHolder, position: Int) {
        val drink = drink[position]
        holder.displayDrinks(drink)
        holder.binding.ivDrink.setOnClickListener {
           val nav =  DrinkByCategoryFragmentDirections.
            actionDrinkByCategoryFragmentToDetailFragment(drink.idDrink.toInt())
            it.findNavController().navigate(nav)
        }
    }

    override fun getItemCount(): Int = drink.size

    fun updateDrink(drinks: List<Drink>) {
        this.drink.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(drinks)
            notifyItemRangeInserted(0, size)
        }
    }
}