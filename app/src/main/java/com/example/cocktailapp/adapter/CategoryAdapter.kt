package com.example.cocktailapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailapp.databinding.ItemCategoryBinding
import com.example.cocktailapp.model.response.CategoryResponse.Category
import com.example.cocktailapp.view.CategoryFragmentDirections

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var category = mutableListOf<Category>()

    class CategoryViewHolder(
         val binding: ItemCategoryBinding,
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun displayCategory(category: Category) = with(binding) {
            tvCategory.text = category.strCategory
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
        CategoryViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
       val drinkCategory = category[position]
        holder.displayCategory(drinkCategory)
        holder.binding.tvCategory.setOnClickListener {
            val nav = CategoryFragmentDirections
                .actionCategoryFragmentToDrinkByCategoryFragment(drinkCategory.strCategory)
            it.findNavController().navigate(nav)
        }
    }

    override fun getItemCount(): Int = category.size

    fun updateCategory(categories: List<Category>){
        this.category.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(categories)
            notifyItemRangeInserted(0, size)
        }
    }
}