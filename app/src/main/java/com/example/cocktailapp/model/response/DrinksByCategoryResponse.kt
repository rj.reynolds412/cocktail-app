package com.example.cocktailapp.model.response


import com.google.gson.annotations.SerializedName

data class DrinksByCategoryResponse(
    val drinks: List<Drink>
){
    data class Drink(
        val idDrink: String = "",
        val strDrink: String = "",
        val strDrinkThumb: String = ""
    )
}