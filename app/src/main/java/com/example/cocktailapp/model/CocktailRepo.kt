package com.example.cocktailapp.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CocktailRepo(private val cocktailService: CocktailService) {

    suspend fun getCategories(category: String) = withContext(Dispatchers.IO) {
        cocktailService.getCategory(category)
    }

    suspend fun getDrinkByCategory(categoryDrink: String) = withContext(Dispatchers.IO) {
        cocktailService.getDrinkByCategory(categoryDrink)
    }

    suspend fun getDetailsById(detailsById : Int) = withContext(Dispatchers.IO) {
        cocktailService.getDetailsById(detailsById)
    }
}