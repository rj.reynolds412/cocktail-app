package com.example.cocktailapp.model

import android.util.Log
import com.example.cocktailapp.model.response.CategoryResponse
import com.example.cocktailapp.model.response.DetailByIdResponse
import com.example.cocktailapp.model.response.DrinksByCategoryResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.IOException


interface CocktailService {

    companion object {
        private const val DRINK_CATEGORY_ENDPOINT = "api/json/v1/1/list.php"
        private const val DRINK_IN_CATEGORY_ENDPOINT = "api/json/v1/1/filter.php"
        private const val DETAIL_BY_ID_ENDPOINT = "api/json/v1/1/lookup.php"
        private const val QUERY_NAME = "c"
        private const val QUERY = "i"

    }

    @GET(DRINK_CATEGORY_ENDPOINT)
    suspend fun getCategory(@Query(QUERY_NAME) category: String = "list"): CategoryResponse

    @GET(DRINK_IN_CATEGORY_ENDPOINT)
    suspend fun getDrinkByCategory(@Query(QUERY_NAME) categoryDrink: String): DrinksByCategoryResponse

    @GET(DETAIL_BY_ID_ENDPOINT)
    suspend fun getDetailsById(@Query(QUERY) detailsByID: Int): DetailByIdResponse

}

internal class LoggingInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val t1 = System.nanoTime()
        Log.d("OkHttp", java.lang.String.format("Sending request %s on %s%n%s",
            request.url(), chain.connection(), request.headers()))
        Log.d("OkHttp", request.toString())
        val response: Response = chain.proceed(request)
        val t2 = System.nanoTime()
        Log.d("OkHttp", java.lang.String.format("Received response for %s in %.1fms%n%s",
            response.request().url(), (t2 - t1) / 1e6, response.headers()))
        Log.d("OkHttp", response.toString())
        Log.d("OkHttp", response.body().toString())
        return response
    }
}