package com.example.cocktailapp.model.response

import com.google.gson.annotations.SerializedName


data class CategoryResponse(
    @SerializedName("drinks")
    val categories: List<Category> = emptyList()
){
    data class Category(
        val strCategory: String = ""
    )
}