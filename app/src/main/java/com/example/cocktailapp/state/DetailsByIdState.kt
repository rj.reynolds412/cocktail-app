package com.example.cocktailapp.state

import com.example.cocktailapp.model.response.DetailByIdResponse
import com.example.cocktailapp.model.response.DetailByIdResponse.*

data class DetailsByIdState(
    val isLoading: Boolean = false,
    val detailsById: Details = Details(),
    val error: String? = null
)