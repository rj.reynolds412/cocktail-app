package com.example.cocktailapp.state

import com.example.cocktailapp.model.response.CategoryResponse.Category

data class CategoryState(
    val isLoading: Boolean = false,
    val categoryList: List<Category> = emptyList(),
    val error: String? = null,
)
