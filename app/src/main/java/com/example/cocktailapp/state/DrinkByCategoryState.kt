package com.example.cocktailapp.state

import com.example.cocktailapp.model.response.DrinksByCategoryResponse.Drink

data class DrinkByCategoryState(
    val isLoading: Boolean = false,
    val drinkList: List<Drink> = emptyList(),
    val error : String? = null
)
