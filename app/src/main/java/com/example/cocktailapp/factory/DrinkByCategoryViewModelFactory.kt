package com.example.cocktailapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.cocktailapp.domain.GetDrinkByCategoryUseCase
import com.example.cocktailapp.viewModel.DrinkByCategoryViewModel

class DrinkByCategoryViewModelFactory(private val getDrinkByCategoryUseCase: GetDrinkByCategoryUseCase)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
       if (modelClass.isAssignableFrom(DrinkByCategoryViewModel::class.java)){
           @Suppress("UNCHECKED_CAST")
           return DrinkByCategoryViewModel(getDrinkByCategoryUseCase) as T
       }
        throw IllegalArgumentException("Unable to construct ViewModel")
    }
}