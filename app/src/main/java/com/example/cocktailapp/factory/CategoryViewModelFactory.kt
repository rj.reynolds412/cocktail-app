package com.example.cocktailapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.cocktailapp.domain.GetCategoryUseCase
import com.example.cocktailapp.viewModel.CategoryViewModel

class CategoryViewModelFactory( private val getCategoryUseCase: GetCategoryUseCase)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CategoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CategoryViewModel(getCategoryUseCase) as T
        }
        throw IllegalArgumentException("Unable to construct ViewModel")
    }
}